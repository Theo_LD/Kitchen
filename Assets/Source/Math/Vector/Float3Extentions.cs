﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace LibelludDigital.Math
{
    public static class Float3Extentions
    {
        // Vector3

        public static Vector3 XYZ(this Vector3 v3)
        {
            return v3;
        }

        public static Vector3 XZY(this Vector3 v3)
        {
            return new Vector3(v3.x, v3.z, v3.y);
        }

        public static Vector3 YXZ(this Vector3 v3)
        {
            return new Vector3(v3.y, v3.x, v3.z);
        }

        public static Vector3 YZX(this Vector3 v3)
        {
            return new Vector3(v3.y, v3.z, v3.x);
        }
        public static Vector3 ZXY(this Vector3 v3)
        {
            return new Vector3(v3.z, v3.x, v3.y);
        }

        public static Vector3 ZYX(this Vector3 v3)
        {
            return new Vector3(v3.z, v3.y, v3.x);
        }

        // Vector4

        public static Vector3 XYZ(this Vector4 v4)
        {
            return new Vector3(v4.x, v4.y, v4.z);
        }

        public static Vector3 XZY(this Vector4 v4)
        {
            return new Vector3(v4.x, v4.z, v4.y);
        }

        public static Vector3 YXZ(this Vector4 v4)
        {
            return new Vector3(v4.y, v4.x, v4.z);
        }

        public static Vector3 YZX(this Vector4 v4)
        {
            return new Vector3(v4.y, v4.z, v4.x);
        }
        public static Vector3 ZXY(this Vector4 v4)
        {
            return new Vector3(v4.z, v4.x, v4.y);
        }

        public static Vector3 ZYX(this Vector4 v4)
        {
            return new Vector3(v4.z, v4.y, v4.x);
        }
    }
}