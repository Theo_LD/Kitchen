﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntityView : MonoBehaviour {

    public static float MOVEMENT_RESOLUTION_TICK_DURATION = 0.5f;
    public static float COMBINAISON_RESOLUTION_TICK_DURATION = 0.25f;
    public static float STATE_RESOLUTION_TICK_DURATION = 0.25f;



    public EntityModel model;

    public void Spawn(float v) {
        //transform.localScale = Vector3.zero;
        //transform.DOMove(transform.position, v);
        //transform.Translate(Vector3.right * -1.5f);
        //transform.DOScale(new Vector3(0.5f, 0.5f, 0.5f), v);
    }

    public void OnMove(int x, int y, bool bumped) {
        GameObject tile;
        try {
            tile = GameView.instance.lines[model.positionY].transform.GetChild(model.positionX).gameObject;
            transform.DOMove(tile.transform.position, MOVEMENT_RESOLUTION_TICK_DURATION).SetEase(Ease.Linear);
        }
        catch {
            transform.DOMove(transform.position + new Vector3(x, y, 0), MOVEMENT_RESOLUTION_TICK_DURATION).SetEase(Ease.Linear);
        }
        if (bumped) {
            transform.DOScale(1.0625f, MOVEMENT_RESOLUTION_TICK_DURATION * 0.5f).SetEase(Ease.OutCubic).SetLoops(2, LoopType.Yoyo);
        }
    }


    public void OnStateChanged() {
        switch (model.state) {
            case EntityModel.StateEnum.ALIVE:
                break;
            case EntityModel.StateEnum.TRASHED:
                transform.DOKill();
                transform.DOScale(0f, STATE_RESOLUTION_TICK_DURATION);
                transform.DORotate(new Vector3(0, 0, -180), STATE_RESOLUTION_TICK_DURATION);
                model.eMoved = null;
                model.eOnStateChange = null;
                Destroy(this.gameObject, STATE_RESOLUTION_TICK_DURATION * 2);
                break;
            case EntityModel.StateEnum.EATEN_GOOD:
                model.eMoved = null;
                model.eOnStateChange = null;
                transform.DOKill();
                transform.DOScale(Vector3.zero, STATE_RESOLUTION_TICK_DURATION).SetEase(Ease.Linear).onComplete = delegate () {
                    model.eMoved = null;
                    GameView.instance.EntityEaten(model.positionY, true);
                    Destroy(this.gameObject);
                };
                break;
            case EntityModel.StateEnum.EATEN_BAD:
                model.eMoved = null;
                model.eOnStateChange = null;
                transform.DOKill();
                transform.DOScale(Vector3.zero, STATE_RESOLUTION_TICK_DURATION).SetEase(Ease.Linear).onComplete = delegate () {
                    model.eMoved = null;
                    Destroy(this.gameObject);
                    GameView.instance.EntityEaten(model.positionY, false);
                };
                break;
        }
    }

    public void Init(EntityModel model) {
        this.model = model;
        switch (model.type) {
            case EntityModel.EntityType.A:
                this.GetComponent<SpriteRenderer>().sprite = PrefabRefs.instance.candyA;
                break;
            case EntityModel.EntityType.B:
                this.GetComponent<SpriteRenderer>().sprite = PrefabRefs.instance.candyB;
                break;
            case EntityModel.EntityType.C:
                this.GetComponent<SpriteRenderer>().sprite = PrefabRefs.instance.candyC;
                break;
        }
        model.eFusioned = OnFusion;
        model.eErased = OnErased;
        model.eMoved = OnMove;
        model.eOnStateChange = OnStateChanged;
    }

    private void OnErased() {
        Destroy(this.gameObject);
    }

    private void OnFusion() {
        this.GetComponent<SpriteRenderer>().sprite = PrefabRefs.instance.fusion;
    }
}
