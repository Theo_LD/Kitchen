﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterModel {
    public EntityModel.EntityType expectedEntity;
    public bool hasEaten = false;
    public MonsterModel() {
        expectedEntity = (EntityModel.EntityType)Random.Range(0, 3); 
    }
}
