﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntityModel {

    public System.Action eSpawned;
    public System.Action eErased;
    public System.Action eFusioned;
    public delegate void OnMoveHandler(int x, int y, bool bumped);
    public OnMoveHandler eMoved;
    public System.Action eOnStateChange;

    

    public int
        positionX,
        positionY;

    public EntityType type;


    public EntityModel() {
        type =  (EntityType) UnityEngine.Random.Range(0,3);
    }

    public enum StateEnum {
        ALIVE,      // The enity is being processed.
        TRASHED,    // The entity was thrown away.
        EATEN_GOOD, // The entity was eaten for the best.
        EATEN_BAD   // The entity was eaten for the worst.
    }

    public StateEnum state = StateEnum.ALIVE;

    public void SetState(StateEnum newState) {
        state = newState;
        if (eOnStateChange != null)
            eOnStateChange();
    }

    public void Spawn(int x, int y) {
        positionX = x;
        positionY = y;

        if (eSpawned != null)
            eSpawned();
    }

    public void Move(int x, int y, bool bumped) {
        positionX += x;
        positionY += y;

        if (eMoved != null)
            eMoved(x, y, bumped);
    }
    
    public enum EntityType {
        A,B,C,FUSION
    }

    internal void Fusion() {
        type = EntityType.FUSION;
        eFusioned();
    }
}
